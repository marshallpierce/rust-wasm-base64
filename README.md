Getting started:

- Install [rustup](https://rustup.rs/) if you don't already have it
- `rustup update`
- `rustup target add wasm32-unknown-unknown --toolchain nightly`
- `cargo +nightly build --target=wasm32-unknown-unknown --release`
- `python -m SimpleHTTPServer 9000` or any other trivial web server
- load [http://localhost:9000](http://localhost:9000)

Borrows liberally from https://www.steveklabnik.com/wasm/demos/semver.html.
