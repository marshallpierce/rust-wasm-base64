#![feature(allocator_api)]

extern crate base64;

use std::mem;
use std::ffi::CStr;
use std::os::raw::c_char;
use std::heap::{Alloc, Heap, Layout};

macro_rules! check {
    ($expr:expr) => (match $expr {
        Ok(val) => val,
        Err(_) => return 0,
    })
}

#[no_mangle]
pub extern "C" fn base64_encode(input: *mut c_char) -> u32 {
    let cstr;
    unsafe {
        cstr = CStr::from_ptr(input);
    };
    let input_str = check!(cstr.to_str());

    let encoded = base64::encode(input_str);

    encoded.len() as u32
}

#[no_mangle]
pub extern "C" fn alloc(size: usize) -> *mut u8 {
    unsafe {
        let layout = Layout::from_size_align(size, mem::align_of::<u8>()).unwrap();
        Heap.alloc(layout).unwrap()
    }
}

#[no_mangle]
pub extern "C" fn dealloc(ptr: *mut u8, size: usize) {
    unsafe {
        let layout = Layout::from_size_align(size, mem::align_of::<u8>()).unwrap();
        Heap.dealloc(ptr, layout);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn alloc_and_leak_something() {
        alloc(16);
    }
}
